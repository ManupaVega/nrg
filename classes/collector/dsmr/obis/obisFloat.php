<?php
namespace collector\dsmr\obis;

use Exception;

class obisFloat implements obisValueType {

    private $size;
    private $mindec;
    private $maxdec;


    public function __construct($size=0, $mindec=0, $maxdec=0) {


    }

    public function getConvertedValue($data) {
        return (float) $this->disectData($data)[0];
    }

    public function getUnit($data) {
        return $this->disectData($data)[1];
    }

    private function disectData($data) {
        $unit=null;

        if (substr($data,0,1) != "(" || substr($data, -1) != ")") {
            throw new Exception("Incorrect data format");
        }
        $data=substr($data,1,-1);

        $asterisk=strpos($data, "*");
        if ($asterisk !== false) {
            $unit=substr($data, $asterisk + 1);
            $data=substr($data, 0, $asterisk);
        }

        return array($data, $unit);

    }
}

