<?php
namespace collector\dsmr\obis;

use DateTime;
use DateTimeZone;
use Exception;

class obisTST implements obisValueType {

    public function __construct() {


    }

    public function getConvertedValue($data) {
        if (substr($data,0,1) != "(" || substr($data, -1) != ")") {
            throw new Exception("Incorrect data format");
        }
        $data=substr($data,1,-1);

        if (substr($data, -1 == "W")) {
            $data=substr($data, 0, -1);
            $tz=new dateTimeZone("CET");
        } else if (substr($data, -1 == "S")) {
            $data=substr($data, 0, -1);
            $tz=new dateTimeZone("CEST");
        } else {
            throw new Exception("Incorrect data format summer/winter time");
        }

        $time=DateTime::createFromFormat("ymdGis", $data);
        if ($time===false) {
            throw new Exception("Error in date/time: " . DateTime::getLastErrors());
        }
        return $time;
    }

    public function getUnit($data) {
        return null;
    }
}
