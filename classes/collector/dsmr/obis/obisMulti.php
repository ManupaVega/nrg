<?php
namespace collector\dsmr\obis;

use Exception;

class obisMulti implements obisValueType {

    private $valueTypes;

    public function __construct(array $valueTypes) {
        $this->valueTypes=$valueTypes;


    }

    public function getConvertedValue($data) {
        return $this->disectData($data)[0];
    }

    public function getUnit($data) {
        return $this->disectData($data)[1];
    }

    private function disectData($data) {
        $values=array();
        $units=array();

        $data=str_replace(")(", "),(", $data);
        $data=explode(",", $data);

        $key=0;
        foreach ($data as $value) {

            if (substr($value,0,1) != "(" || substr($value, -1) != ")") {
                throw new Exception("Incorrect data format");
            }

            $valueType=$this->valueTypes[$key++];

            $values[]=$valueType->getConvertedValue($value);
            $units[]=$valueType->getUnit($value);

        }

        return array($values, $units);

    }
}
