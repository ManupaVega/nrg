<?php
namespace collector\dsmr\repository;

use DateTime;
use DateTimeZone;
use PDO;
use stdClass;

use repository as repositoryInterface;

use db\clause;
use db\db;
use db\insert;
use db\param;
use db\select;

abstract class repository implements repositoryInterface {

    public function __construct() {
    }

    public function load($id) {

    }

    public function save($measurement) {
        $qry=new insert(static::TABLE);

        foreach ($measurement->getData() as $label => $value) {
            $param=new param(":" . $label, $value);
            $qry->addParam($param);
        }
        $qry->execute();
    }

    public function getAll() {

    }

    public function get($field, $value) {


    }

    public function getLast() {
        $qry=new select(static::TABLE);
        $qry->addLimit(1);
        $qry->addOrder("datetime DESC");
        $result=db::query($qry);
        return $result->fetchObject("collector\dsmr\measurement", array(new static));
    }

    public function getLastHour() {
        $qry=new select(static::TABLE);
        $qry->addFields(array("datetime", "current", "currentL1", "currentL2", "currentL3"));
        $qry->addOrder("datetime");
        $qry->where(new clause("datetime >= DATE_SUB(NOW(), INTERVAL 1 HOUR)"));
        $result=db::query($qry);
        $measurements=$result->fetchAll(PDO::FETCH_CLASS,"collector\dsmr\measurement", array(new static));

        $currentL1=new StdClass();
        $currentL1->name="Gebruik Fase 1";
        $currentL1->data=array();

        $currentL2=new StdClass();
        $currentL2->name="Gebruik Fase 2";
        $currentL2->data=array();

        $currentL3=new StdClass();
        $currentL3->name="Gebruik Fase 3";
        $currentL3->data=array();

        $current=new StdClass();
        $current->name="Gebruik Totaal";
        $current->data=array();

        foreach ($measurements as $measurement) {
            $date = DateTime::createFromFormat('Y-m-d H:i:s', $measurement->datetime, new DateTimeZone("UTC"))->getTimestamp()*1000;
            //$date=$measurement->datetime;
            $currentL1->data[]=array($date, (float) $measurement->currentL1 * 1000);
            $currentL2->data[]=array($date, (float) $measurement->currentL2 * 1000);
            $currentL3->data[]=array($date, (float) $measurement->currentL3 * 1000);
            $current->data[]=array($date, (float) $measurement->current * 1000);

         }
         return array(
            $currentL1,
            $currentL2,
            $currentL3,
            $current
         );
    }


}

