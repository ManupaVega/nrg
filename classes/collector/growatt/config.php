<?php
namespace collector\growatt;

class config {

    private $index;
    private $value;


    public function __construct($index, $value) {
        $this->index = $index;
        $this->value = $value;

    }

    public function display() {
        return $this->lookupDesc() . ": " . $this->value;
    }

    public function getName() {
        return $this->lookupName();
    }

    public function getValue() {
        return $this->value;
    }

    public function getIndex() {
        return $this->index;
    }

    private function lookupName() {
        $index = $this->index;
        $config = [
            0x04 => "interval",
            0x05 => "range1",
            0x06 => "range2",
            0x06 => "dataloggerid",
            0x08 => "serial",
            0x09 => "hwversion",
            0x0b => "ftpcred",
            0x0c => "dns",
            0x0e => "localip",
            0x10 => "mac",
            0x0f => "Localport",
            0x11 => "serverip",
            0x12 => "serverport",
            0x13 => "server",
            0x14 => "type",
            0x15 => "swversion",
            0x16 => "version",
            0x1e => "timezone",
            0x1f => "date",
        ];

        if (isset($config[$index])) {
            return $config[$index];
        } else {
            return "0x" . dechex($index) . "_unknown";
        }
    }

    private function lookupDesc() {
        $index = $this->index;
        $config = [
            0x04 => "Update Interval",
            0x05 => "Modbus Range low",
            0x06 => "Modbus Range high",
            0x06 => "Modbus Address",
            0x08 => "Serial Number",
            0x09 => "Hardware Version",
            0x0b => "FTP credentials",
            0x0c => "DNS",
            0x0e => "Local IP",
            0x0f => "Local Port",
            0x10 => "Mac Address",
            0x11 => "Server IP",
            0x12 => "Server Port",
            0x13 => "Server",
            0x14 => "Device Type",
            0x15 => "Software Version",
            0x16 => "Hardware Version",
            0x1e => "Timezone",
            0x1f => "Date"
        ];

        if (isset($config[$index])) {
            return $config[$index];
        } else {
            return "Unknown (0x" . dechex($index) . ").";
        }
    }

    public static function createFromTLV($msg) {
        $cfg = array();

        $data = unpack("C4/nsize/C4type/a10serial/a10ident/a10ident2/C*", $msg);
        $size = $data["size"];
        
        // cut off header
        $msg = substr($msg, 40);
        while(strlen($msg) > 2) {
            $data = unpack("ntype/nlength/a*value", $msg);
            $type = $data["type"];
            $length = $data["length"];
            $value = substr($data["value"], 0, $length);

            $cfg[$type] = new self($type, $value);
            $msg=substr($msg, $data["length"] + 4);
        }
        return $cfg;
    }

}


?>
