<?php
/**
 * This is some code I used to get started with development before I actually got my inverter
 * it mostly emulates Version 2 of the protocol.
 * All of this is mostly based on @sciurius - https://github.com/sciurius/Growatt-WiFi-Tools
 */
namespace collector\growatt;

define("EWOULDBLOCK", 11);
define("EINPROGRESS", 115);


class testInverter {

    const SERIAL = "TE00000001";
    const IDENT  = "ST00000001";
    const TYPE   = "TEST2019";


    const ANNOUNCE_SENT = 1;
    const ANNOUNCE_RCVD = 2;
    const QUERY_RCVD = 3;
    const QUERY_REPLIED = 4;

    private $server = "";
    private $port = 0;
    private $version;
    private $socket = false;

    private $outstanding = 0;

    private $fin = false;



    public function __construct($server, $version=2) {
        $this->server = $server;
        $this->port = 5279;
        $this->version = $version;

        $this->connect();


    }

    private function connect() {
        /* Create a TCP/IP socket. */
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if ($socket === false) {
            echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        } else {
            echo "OK.\n";
        }

        echo "Attempting to connect to server $this->server on port $this->port...";
        $result = socket_connect($socket, $this->server, $this->port);

        socket_set_nonblock($socket);

        $this->socket = $socket;

    }

    private function receive() {
        $pingtimer = microtime(true) + 180;
        $anntimer = microtime(true) + 30;
        $datatimer = microtime(true);

        while (true) {
            sleep(1);
            if (microtime(true) > $pingtimer) {
                $pingtimer = microtime(true) + 180;
                $this->sendPing();
            }

            if (($this->state==self::ANNOUNCE_SENT) && (microtime(true) > $anntimer)) {
                $anntimer = microtime(true) + 30;
                $this->outstanding++;
                $this->sendAnnounce();
            } else if ($this->state==self::QUERY_REPLIED && (microtime(true) > $datatimer)) {
                $datatimer = microtime(true) + 10;
                $this->sendEnergyData();
            }

            if ($this->outstanding > 15) {
                break;
            }

            if (($buf = socket_read($this->socket, 2048, PHP_BINARY_READ))===false) {
                $error = socket_last_error($this->socket);
                if ($error != EWOULDBLOCK && $error != EINPROGRESS) {
                    echo "socket_read() failed: reason: " . socket_strerror($error) . "\n";
                    break;
                }
            } else if (strlen($buf) > 0) {
                yield $buf;
            }
        }
        echo "Closing socket...";
        socket_close($this->socket);
        echo "OK.\n\n";
    }

    public function run() {
        // Startup
        $this->sendPing();


        // Send Announce
        $this->sendAnnounce();

        $this->state=self::ANNOUNCE_SENT;
        foreach ($this->receive() as $buf) {
            if ($this->fin) {
                break;
            }
            while (strlen($buf) > 0) {

                $msg=message::createFromBuffer($buf);
                if ($msg) {
                    $msg->decode();

                    if ($msg->getType = message::PING) {
                        // Not checking reply or whether we're actually waiting for a ping
                        echo "Ping reply received\n";
                    }

                    if ($this->state==self::ANNOUNCE_SENT &&
                            $msg->getType = message::ANNOUNCE &&
                            $msg->isAck()) {
                        echo "ACK to Announce Received\n";
                        $this->state=self::ANNOUNCE_RCVD;
                        $this->outstanding = 0;
                    } else if ($this->state==self::ANNOUNCE_RCVD &&
                            $msg->getType = message::QUERY) {
                        $this->state=self::QUERY_RCVD;
                        $data = $msg->decodeQueryRequest();
                        echo "Reveived Query request for config items " . $data["first"] . " to " . $data["last"] . " acknowledging...";
                        $ack = new message();
                        $ack->create(2, message::QUERY, $data["serial"] . pack("n2", $data["first"], $data["last"]));
                        socket_write($this->socket, $ack->getMsg(), $ack->getSize());

                        $this->sendConfig($data["first"], $data["last"]);
                        $this->state=self::QUERY_REPLIED;
                    }

                    $buf = $msg->getRemaining();

                }


            }
        }

    }

    private function sendPing() {
        $size = strlen(self::SERIAL) + 2;
        //$ping = pack("c4nc2", 0x00,0x01,0x00,0x02,$size,0x01, 0x16) . $this->serial;

        $ping = new message();
        $ping->create($this->version, message::PING, self::SERIAL);

        print("Sending Ping\n");
        socket_write($this->socket, $ping->getMsg(), $ping->getSize());


    }

    private function sendAnnounce() {
        $announce = self::SERIAL . self::IDENT;
        $announce .= pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x2c, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00);
        $announce .= pack("C*", 0xff, 0xff, 0xff, 0x00, 0x01, 0x11, 0x70, 0x17, 0x70);
        $announce .= pack("C*", 0x30, 0x43, 0x30, 0x2e, 0x39, 0x20, 0x30, 0x00, 0x44, 0x30, 0x2e, 0x39, 0x20, 0x00, 0x01, 0x00, 0x00, 0x0d, 0xac, 0x00);
        $announce .= pack("C*", 0x1e, 0x07, 0x35, 0x0a, 0x55, 0x12, 0x91, 0x13, 0x9c);
        $announce .= self::IDENT;
        $announce .= pack("C*", 0x00, 0x10, 0xf1, 0x71, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x35);
        $announce .= pack("C*", 0x0a, 0x55, 0x12, 0x91, 0x13, 0x9c, 0x06, 0x40, 0x0a, 0x8c, 0x11, 0xf8, 0x15, 0x18, 0x08, 0x53);
        $announce .= pack("C*", 0x02, 0x03, 0x00, 0x2d, 0x00, 0x59, 0x07, 0xdc, 0x00, 0x01, 0x00, 0x02, 0x00, 0x10, 0x00, 0x39);
        $announce .= pack("C*", 0x00, 0x00, 0x03, 0xe8, 0x03, 0xe8, 0x00, 0x64, 0x00, 0x64, 0x03, 0xe8, 0x03, 0xe8, 0x00, 0x00);
        $announce .= pack("C*", 0x00, 0x00);
        $announce .= "Growatt Inverter";
        $announce .= self::TYPE;
        $announce .= pack("C*", 0x00, 0x00, 0x00, 0x05, 0x01, 0x30, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $announce .= pack("C*", 0x00, 0x00, 0x00, 0x01, 0x13, 0xa6, 0x00, 0xc8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $announce .= pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);

        $size = strlen($announce) + 2;
        //$ping = pack("c4nc2", 0x00,0x01,0x00,0x02,$size,0x01, 0x16) . $this->serial;

        $msg = new message();
        $msg->create($this->version, message::ANNOUNCE, $announce);

        print("Sending Announce\n");
        socket_write($this->socket, $msg->getMsg(), $msg->getSize());


    }

    private function sendConfig($first, $last) {
        $config = array(
            0x04    => pack("C*", 0x35),
            0x05    => pack("C*", 0x31),
            0x06    => pack("C*", 0x33, 0x32),
            0x08    => pack("C*", self::SERIAL),
            0x0a    => pack("C*", 0x30),
            0x0b    => pack("C*", 0x30, 0x23, 0x30, 0x23, 0x30, 0x2e, 0x30,
                                  0x2e, 0x30, 0x2e, 0x30, 0x23, 0x30, 0x23),
            0x0d    => pack("C*", 0x32),
            0x0e    => pack("C*", 0x31, 0x39, 0x32, 0x2e, 0x31,0x36,0x38,
                                  0x2e, 0x31, 0x2e, 0x31, 0x34, 0x39),
            0x0f    => pack("C*", 0x38, 0x38, 0x39, 0x36),
            0x10    => pack("C*", 0x41, 0x43, 0x3a, 0x43, 0x46, 0x3a, 0x32, 0x33, 0x3a,
                                  0x33, 0x44, 0x3a, 0x38, 0x31, 0x3a, 0x45, 0x35),
            0x11    => pack("C*", 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x2e, 0x67, 0x72,
                                  0x6f, 0x77, 0x61, 0x74, 0x74, 0x2e, 0x63, 0x6f, 0x6d),
            0x12    => pack("C*", 0x35, 0x32, 0x37, 0x39),
            0x13    => pack("C*", 0x73, 0x65, 0x72, 0x76, 0x65, 0x72, 0x2e, 0x67, 0x72,
                                  0x6f, 0x77, 0x61, 0x74, 0x74, 0x2e, 0x63, 0x6f, 0x6d),
            0x14    => pack("C*", 0x33, 0x2e, 0x31, 0x2e, 0x30, 0x2e, 0x30)
        );

        //$queryAck = pack("c*", 0x0, $start, 0x0, $finish);
        //$msg = new message();
        //$msg->create(2, message::QUERY, $queryAck);

        //print("Sending config ack:\n");
        //socket_write($this->socket, $msg->getMsg(), $msg->getSize());

        for ($item = $first; $item<=$last; $item++) {
            if (array_key_exists($item, $config)) {
                $packet = self::SERIAL;
                $packet .= pack("C*", 0x0, $item);
                $packet .= pack("n", strlen($config[$item]));
                $packet .= $config[$item];
                $msg = new message();
                $msg->create($this->version, message::QUERY, $packet);

                print("Sending config item $item\n");
                socket_write($this->socket, $msg->getMsg(), $msg->getSize());
            }
        }


    }


    private function sendEnergyData() {
        echo "Sending Energy Data...\n";
        if ($this->version == 2) {
            $data = $this->getEnergyDataV2();
        } else if ($this->version == 5) {
            $data = $this->getEnergyDataV5();
        }
        socket_write($this->socket, $data, strlen($data));
    }

    private function getEnergyDataV2() {
        $data=pack("C*", 0x00, 0x01, 0x00, 0x02, 0x00, 0xd9, 0x01, 0x04, 0x41, 0x48, 0x34, 0x34, 0x34, 0x36, 0x30, 0x34);
        $data.=pack("C*", 0x37, 0x37, 0x4f, 0x50, 0x32, 0x34, 0x35, 0x31, 0x30, 0x30, 0x31, 0x37, 0x00, 0x00, 0x00, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x2c, 0x00, 0x01, 0x00, 0x00, 0x0a, 0xb1, 0x09, 0xf9, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x15, 0x63, 0x00, 0x05, 0x00, 0x00, 0x0a, 0xb1, 0x00, 0x00, 0x08);
        $data.=pack("C*", 0x44, 0x13, 0x84, 0x09, 0x04, 0x00, 0x03, 0x00, 0x00, 0x02, 0xb4, 0x08, 0xfc, 0x00, 0x04, 0x00);
        $data.=pack("C*", 0x00, 0x03, 0x98, 0x09, 0x08, 0x00, 0x03, 0x00, 0x00, 0x02, 0xb5, 0x00, 0x00, 0x00, 0xf3, 0x00);
        $data.=pack("C*", 0x00, 0x01, 0xc3, 0x00, 0x03, 0x0e, 0x47, 0x01, 0x6f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x73, 0x0b, 0x4e, 0x0b, 0x48, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x2d, 0x00, 0x59, 0x4e, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf3, 0x00, 0x00, 0x01, 0xbf, 0x00, 0x00, 0x01, 0xbf, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $data.=pack("C*", 0x04, 0x00, 0x01, 0x11, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        $data.=pack("C*", 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
        return $data;
    }

    private function getEnergyDataV5() {

    }

}
