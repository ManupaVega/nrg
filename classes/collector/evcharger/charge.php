<?php
namespace collector\evcharger;

use repository;
use DateTime;

class charge {

    private $id = 0;

    public function __construct(
        private repository  $repo, 
        private int         $session_id, 
        private DateTime    $start, 
        private float       $start_power, 
        private null|DateTime    $end = null, 
        private null|float       $end_power = null
    ) {}

    public function save() {
        $this->repo->save($this);
    }

    public function update() {
        $this->repo->update($this);
    }

    public function getData() {
        return array(
            "session_id"    => $this->session_id,
            "start"         => $this->start,
            "end"           => $this->end,
            "start_power"   => $this->start_power,
            "end_power"     => $this->end_power
        );
    }

    public function setId(int $id) {
        $this->id = $id;
    }
    
    public function getId() {
        return $this->id;
    }


    public static function getForSession(repository $repo, session $session) {
        return $repo->getForSession($session);
    }

    public function getEndPower() {
        return (float) $this->end_power;
    }

    public function hasEnded() {
        return !is_null($this->end);
    }

    public function end() {
        $repo = new \collector\evcharger\repository\power();
        
        $this->end = new DateTime();
        $this->end_power=$repo->getMaxTotal();

        $this->update();
    }

    public static function getCurrent(repository $repo) {
        return $repo->getCurrent();
    }

    public static function start($repo) {
        $powerRepo = new \collector\evcharger\repository\power();
        $sessionRepo = new \collector\evcharger\repository\session();
        $session = session::getCurrent($sessionRepo);
        if (!$session) {
            $session = session::start(new \collector\evcharger\repository\session(), "UNKNOWN");
        }

        $start = new DateTime();
        $start_power=(float) $powerRepo->getMaxTotal();

        $charge = new self($repo, $session->getId(), $start, $start_power);
        $repo->save($charge);

        return $charge;

    }

}
