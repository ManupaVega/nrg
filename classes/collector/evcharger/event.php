<?php
namespace collector\evcharger;

use repository;
use DateTime;

class event {

    private $datetime;

    public function __construct(private repository $repo, private string $event, private string $data) {
        $this->datetime = new DateTime("now");
    }

    public function save() {
        $this->repo->save($this);
    }

    public function getData() {
        return array(
            "timestamp" => $this->datetime,
            "action"    => $this->event,
            "data"      => $this->data
        );
    }
}
