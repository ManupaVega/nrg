<?php
namespace collector\evcharger;

use repository;
use DateTime;

class session {

    private $id = 0;

    public function __construct(
        private repository  $repo, 
        private DateTime    $start, 
        private float       $start_power, 
        private string      $start_user,
        private null|DateTime    $end = null, 
        private null|float       $end_power = null,
        private null|string      $end_user = null
    ) {}

    public function save() {
        $this->repo->save($this);
    }

    public function getData() {
        return array(
            "start"         => $this->start,
            "end"           => $this->end,
            "start_user"    => $this->start_user,
            "end_user"      => $this->end_user,
            "start_power"   => $this->start_power,
            "end_power"     => $this->end_power
        );
    }

    public function setId(int $id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function end(string $user) {
        $this->end = new DateTime();
        $this->end_user = $user;

        $charges = $this->getCharges();
        $this->end_power = $this->start_power;
        if ($charges) {
            foreach($charges as $charge) {
                if(!$charge->hasEnded()) {
                    $charge->end();
                }
                $this->end_power = max($this->end_power, $charge->getEndPower());
            }
        }
        $this->repo->update($this);
    }

    public static function getCurrent(repository $repo) {
        return $repo->getCurrent();
    }

    public function getCharges() {
        $repo = new \collector\evcharger\repository\charge();
        return charge::getForSession($repo, $this);
    }

    public static function start($repo, $user) {
        $powerRepo = new \collector\evcharger\repository\power();


        $start = new DateTime();
        $start_user = $user;
        $start_power=(float) $powerRepo->getMaxTotal();

        $session = new self($repo, $start, $start_power, $start_user);
        $repo->save($session);

        return $session;

    }
}
