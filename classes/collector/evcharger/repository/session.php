<?php
namespace collector\evcharger\repository;

use DateTime;
use db\db;
use db\select;
use db\clause;
use repository as repositoryInterface;
use PDO;

class session extends repository implements repositoryInterface {
    const TABLE="evcharger_session";
    protected static $OBJECT=\collector\evcharger\session::class;

    public function getCurrent() {
        $qry = new select(static::TABLE);
        $qry->where(new clause("end IS NULL"));
        $qry->addLimit(1);
        $result=db::query($qry);
        $row = $result->fetch(PDO::FETCH_OBJ);
        $session = null;
        if ($row) {
            $start = new DateTime($row->start);
            $session = new static::$OBJECT($this, $start, $row->start_power, $row->start_user, null, null, null);
            $session->setId($row->id);
        }
        return $session;
    }
}

