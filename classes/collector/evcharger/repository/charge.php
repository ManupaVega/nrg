<?php
namespace collector\evcharger\repository;

use DateTime;
use repository as repositoryInterface;
use collector\evcharger\session;
use db\db;
use db\select;
use db\clause;
use db\param;
use PDO;

class charge extends repository implements repositoryInterface {
    const TABLE="evcharger_charge";
    protected static $OBJECT=\collector\evcharger\charge::class;

    public function getForSession(session $session) {
        $qry = new select(static::TABLE);
        $qry->where(new clause("session_id=:sessionId"));
        $qry->addParam(new param(":sessionId", (int) $session->getId(), PDO::PARAM_INT));
        $result=db::query($qry);
        $rows = $result->fetchAll(PDO::FETCH_OBJ);
        $charges = array();
        foreach ($rows as $row) {
            $start = new DateTime($row->start);
            $end = empty($row->end) ? null : new DateTime($row->end);
            $charge = new static::$OBJECT($this, $row->session_id, $start, $row->start_power, $end, $row->end_power);
            $charge->setId($row->id);
            $charges[] = $charge;
        }
        return $charges;
    }

    public function getCurrent() {
        $qry = new select(static::TABLE);
        $qry->where(new clause("end IS NULL"));
        $qry->addLimit(1);
        $result=db::query($qry);
        $row = $result->fetch(PDO::FETCH_OBJ);
        if ($row) {
            $start = new DateTime($row->start);
            $charge = new static::$OBJECT($this, $row->session_id, $start, $row->start_power, null, null, null);
            $charge->setId($row->id);
        }
        return $charge ?? null;
    }
}

