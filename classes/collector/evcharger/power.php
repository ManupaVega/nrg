<?php
namespace collector\evcharger;

use repository;
use DateTime;

class power {

    private $datetime;

    public function __construct(private repository $repo, private float $power, private float $total) {
        $this->datetime = new DateTime("now");
    }

    public function save() {
        $this->repo->save($this);
    }

    public function getData() {
        return array(
            "timestamp" => $this->datetime,
            "power"     => $this->power,
            "total"     => $this->total
        );
    }

    public static function getMaxTotal(repository $repo) {
        return (int) $repo->getMaxTotal();
    }
}
