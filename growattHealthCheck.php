#!/usr/bin/php
<?php
require_once("include/include.php");

use collector\growatt\repository\memory;

$db=new memory();

if ($db->getHealth(60)) {
    exit(0);
} else {
    exit(100);
}

?>
