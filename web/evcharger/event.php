<?php

ini_set("display_errors", true);
error_reporting(E_ALL);
set_include_path("../..");
require_once("include/include.php");
require_once("include/init.php");

$request=new request();
$action = $request->var("action");
$data = $request->var("data");

$repo = new collector\evcharger\repository\events();
$event = new collector\evcharger\event($repo, $action, $data);
$event->save();

?>
