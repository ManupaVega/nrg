<?php

ini_set("display_errors", true);
error_reporting(E_ALL);
set_include_path("../..");
require_once("include/include.php");
require_once("include/init.php");

use collector\evcharger\session;

$request =  new request();
$repo =     new collector\evcharger\repository\session();
$action =   $request->var("action");
$user =     $request->var("user");

$return = array();
$session = session::getCurrent($repo);
if ($session) {
    $session->end($user);
    $return["end"] = array("session_id" => $session->getId(), "session_data" => $session->getData());
}

if ($action == "start") {
    $session = session::start($repo, $user);
    $return["start"] = array("session_id" => $session->getId(), "session_data" => $session->getData());
}


echo json_encode($return);
?>
