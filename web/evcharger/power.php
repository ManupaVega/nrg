<?php
ini_set("display_errors", true);
error_reporting(E_ALL);
set_include_path("../..");
require_once("include/include.php");
require_once("include/init.php");

use collector\evcharger\charge;
$repo = new collector\evcharger\repository\power();

$request=new request();
$action = $request->var("action");


if ($action == "start" || $action == "stop") {
    $repo = new collector\evcharger\repository\charge();
    $charge = charge::getCurrent($repo);
    if ($charge) {
        $charge->end();
    }

    if ($action == "start") {
        $charge = charge::start($repo);
    }
}

$power = new collector\evcharger\power($repo, $request->var("power"), $request->var("total"));
$power->save();
?>
