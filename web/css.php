<?php
header("Content-Type: text/css");
?>
body {
    margin: 0;
}

div.row {
    clear: both;
    overflow: hidden;

}

nav.dashboard {
    margin: 0;
    padding: 0 0 0 15px;
    width: 100%;
    background: black;
    color: white;
    overflow: hidden;
}

nav.dashboard ul {
    margin: 0;
    list-style: none;
    clear: both;
}
nav.dashboard li {
    margin: 0;
    float: left;
    border none;
    border-left: 1px solid white;
    border-right: 1px solid white;

}

nav.dashboard a {
    padding: 8px 16px;
    display: block;
    line-height: 22px;
    text-decoration: none;
    font-family: sans-serif;
    font-size: 18px;
    color: white;
}

nav.dashboard a:hover {
    background: #aaa;
    color: white;
}
