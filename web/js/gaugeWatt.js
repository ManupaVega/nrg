function gaugeWatt(container, title, gaugeTitle, max) {
    return new Highcharts.Chart({
        chart: {
            renderTo: container,
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: true
        },

        title: {
            text: title
        },
        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#FFF'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {
                // default background
            }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        // the value axis
        yAxis: {
            min: 0,
            max: max,

            minorTickInterval: null,

            tickPixelInterval: 30,
            tickInterval: max/30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 5,
                rotation: 'auto'
            },
            title: {
                text: gaugeTitle
            },
            plotBands: [{
                from: 0,
                to: max/5,
                color: '#55BF3B' // green
            }, {
                from: max/5,
                to: max/2.5,
                color: '#DDDF0D' // yellow
            }, {
                from: max/2.5,
                to: max,
                color: '#DF5353' // red
            }]
        },

        series: [{
            name: 'Current usage',
            data: [0],
            tooltip: {
                valueSuffix: 'Watt'
            }, 
            yAxis: 0
        }]
    });
}
