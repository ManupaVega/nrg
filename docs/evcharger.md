EVCHARGER API
=============

NRG includes a simple API that can be used to keep track of EV Chargers.

event
-----
**POST** `evcharger/event.php`
### parameters: ###
```
action = string
data = string
```
Stored in MySQL `evcharger_event` table with a timestamp. Meant to store actions like 'Charging started', 'Charging stopped', 'Car plugged in', etc.

session
-------
**POST** `evcharger/session.php`
### parameters: ###
```
action = [start|stop]
user = string description of user
```
A session is started when a user authenticates against the EV Charger, for example using an RFID tag. The session ends when the user de-authenticates.
There is always a maximum of 1 session active. All previous sessions are closed when a new session is started.

power
-----
**POST** `evcharger/power.php`
### parameters: ###
```
action = start|charging|stop
power = float currently used power (in W)
total = float total used power (in Wh)
```

With this a "charge" is started and stopped, and intermediate (e.g. every minute) statistics are recorded.
A charge is always associated with a session. If there is no active session, a new one will be started with a blank user.
