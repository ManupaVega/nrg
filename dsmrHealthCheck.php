#!/usr/bin/php
<?php
require_once("include/include.php");

use collector\dsmr\repository\memory;

$db=new memory();

if ($db->getHealth(10)) {
    exit(0);
} else {
    exit(100);
}

?>
