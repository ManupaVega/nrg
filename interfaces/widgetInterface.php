<?php
interface widgetInterface {
    public function __toString();
    public function getJS();
    public function getCSS();
    public function getUpdate();
}
